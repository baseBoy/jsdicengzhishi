﻿/*
什么是跨域：
只要是协议、域名、端口有任意一个不同的url，就是跨域
例如：协议指的是http或者https协议
     域名就是www.baidu.com和www.baidu.cn就是不同的域名
     
 3000或者8080端口它们都是不用的端口
只要其中一个不一样的，
就是跨域了

为什么会跨域？

拒绝跨域请求是浏览器为了保护用户的安全的一种策略
为了保证安全性
，防止CSRF攻击。
CSRF(Cross-site request forgery),中文名称跨站请求伪造
可以这么理解CSRF:
攻击者盗用了你的身份，以你的名义发送恶意请求。
CSRF能够做的事情包括：
以你名义发送邮件，发消息，盗取你的账号，
甚至于购买商品，虚拟货币转账......造成的问题包括
：个人隐私泄露以及财产安全。

如何解决跨域？
解决跨域的办法有N种，做常用的主要以下几种
：

document.domain + iframe (只有在主域相同的时候才能使用该方法)

动态创建script JSONP
location.hash + iframe
window.name + iframe
postMessage
（HTML5中的XMLHttpRequest Level 2中的API）
CORS
web sockets

- 跨域资源共享（CORS）
   
项目上线后解决跨域问题：
     服务器设置响应头信息：
      
  res.writeHead(200, {
         
 "Access-Control-Allow-Origin", "http://192.168.1.1:8080";
         
 "Access-Control-Allow-Credentials", "true";
          "
Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH";
      
    "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Token,Accept, Connection, User-Agent, Cookie";
      
    "Access-Control-Max-Age", "3628800"; 
      
  });
 Access-Control-Allow-Origin"表示的是访问服务端的ip地址及端口号，
也可以设置为*表示所有域都可以通过；
"Access-Control-Allow-Credentials"表示的是跨域的ajax中可以携带cookie，
此时第一项设置不能为*，需指定域名；
"Access-Control-Allow-Methods"表示的是允许跨域的请求方法；

"Access-Control-Allow-Headers"表示的是允许跨域请求包含content-type头；
"Access-Control-Allow-Max-Age"表示的是在3628800秒内，
不需要再发送预检验请求，可以缓存该结果，一般默认。-->
   参考文档：https://www.cnblogs.com/sdcs/p/8484905.html


 本地vue项目解决跨域：详见本地配置代理解决跨域.js
*/