﻿/*
- 在开发环境下配置代理配置表，解决本地项目跨域问题

面试题1：如何在开发环境下解决跨域问题的，
   答：
设置代理配置表：
在 config的index文件的proxyTable中设置请求服务的代理
      
把target 对应的域名设置成要请求的域名，把changeOrigin为true，
     
 axios请求的时候把baseURL='/api'，解决了开发环境下的跨域；

步骤：

1，代理配置表：设置config中的index文件的 proxyTable

  proxyTable:
 {//设置请求服务的代理
      '/api': {
       
 target: "http://localhost:7777/",// 接口的域名 
      
  // secure: false,  // 如果是https接口，需要配置这个参数
   
     changeOrigin: true, // 如果接口跨域，需要进行这个参数配置
     
   pathRewrite: {
          '^/api': ''
      
  }
      }
    },

2， 
配置特定的请求代理到对应的API接口

  调接口的时候：baseURL=启动的服务端口号/api ,
例如：我启动的是8080
  axios.defaults.baseURL='http://localhost:8080/api'

  export let getUser=(id)=>{ 
 
    //相当于请求http://localhost:8080/api/getUser
    return axios.get('getUser',{params:{userId:2}})
 }


   将'localhost:8080/api/getUser'代理到'http://localhost:7777/getUser'

 
  参考文档https://www.jianshu.com/p/a65c7abcb59e
 
   https://www.cnblogs.com/pass245939319/p/9040802.html


*/
